/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package perfectnumber;

import java.util.Scanner;

/**
 *
 * @author Limited
 * @version 03 Dec 2015
 */

public class PerfectNumber {

    /**
     * @param args the command line arguments
     */
    static boolean PerfectNumber(int X)
    {
        if (X < 6) return false;
        
        int T= 0;
        for (int i = 1; i<X; i++)
        {
            if (X % i == 0) T+= i;
            if (T > X) return false;
        }
        
        if (T == X) return true;
        else return false;
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        int PfNum[];   /// Mảng lưu các số hoàn hảo
        PfNum = new int[10];   /// Tin mình đi, có rất ít Số hoàn hảo! :)
        
        int count = 0; /// Đếm số Số hoàn hảo
        int T = 0;     /// Tỏng các số hoàn hảo
        
        Scanner jin = new Scanner(System.in);
        System.out.print("Nhập vào, n = "); int n = jin.nextInt();
        System.out.print("Các số hoàn hảo từ 1 đến " + n + ": ");
        for (int i = 1; i<=n; i++)
        {
            if (PerfectNumber(i))
            {
                PfNum[count] = i; count++; T += i;
                System.out.print(i + " ");
            }
                
        }
        
        if (count > 0)
        {
            System.out.println("");
            System.out.println("Tổng các số hoàn hảo: " + T);
        }
        else
            System.out.println("Không có số hoàn hảo nào!");

    }
    
}
